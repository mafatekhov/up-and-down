import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';

import * as R from 'ramda';
import {Observable} from 'rxjs/index';
import {GameSettingsState} from '../shared/reducers/game-settings/game-settings.reducer';
import {AppState, gameScoreSelector, gameSettingsSelector} from '../shared/reducers/root';
import {GameSettingsActions} from '../shared/actions/game-settings/game-settings.actions';
import {PlayerScores} from '../shared/models/player-score';
import {GameScoresState} from '../shared/reducers/game-scores/game-scores.reducer';
import {GameScoreActions} from '../shared/actions/game-score/game-score.actions';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.scss']
})
export class NewGameComponent implements OnInit {

  storeParams$: Observable<GameSettingsState>;
  storePlayersParams$: Observable<GameScoresState>;
  state: GameSettingsState;
  scoresState: GameScoresState;
  gameSettingForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private gameSettingActions: GameSettingsActions,
              private gameScoreActions: GameScoreActions,
              private store: Store<AppState>) {
    this.storeParams$ = store.select(gameSettingsSelector);
    this.storeParams$.subscribe((newState: GameSettingsState) => {
        this.state = newState;
    });
    this.storePlayersParams$ = store.select(gameScoreSelector);
    this.storePlayersParams$.subscribe((newState: GameScoresState) => {
        this.scoresState = newState;
        this.updateState(this.scoresState.players.length, this.gameSettingActions.setNumberOfPlayers);
    });
  }

  ngOnInit() {
    this.gameSettingForm = this.formBuilder.group({
      cardsUpTo: [this.state.cardsUpTo, Validators.compose([Validators.required, Validators.min(1), Validators.max(7)])],
      nameControl: ['Ivan', Validators.compose([Validators.required, Validators.minLength(1)])]
    });
  }

  setCards() {
    const cards = this.gameSettingForm.get('cardsUpTo').value;
    this.updateState(cards, this.gameSettingActions.setCardsUpTo);
  }

  addPlayer() {
    const playerName = this.gameSettingForm.get('nameControl').value;
    this.gameSettingForm.get('nameControl').reset() ;
    const player = new PlayerScores(playerName, 0);
    this.updateState(player, this.gameScoreActions.addPlayer);
  }

  isNameValid(name) {
    return !R.includes(name, this.scoresState.players) && this.scoresState.players.length < 6;
  }

  removePlayer(playerToRemove) {
    this.updateState(playerToRemove, this.gameScoreActions.removePlayer);
  }

  gameSettingsInvalid() {
    return !(this.scoresState.players.length < 2 || this.scoresState.players.length > 6);
  }

  startGame() {
    this.router.navigate(['/game-board']);
  }

  private updateState(newValue, action) {
    const state = action(newValue);
    this.store.dispatch(state);
  }
}
