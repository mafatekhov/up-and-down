
import {Component} from '@angular/core';
import {GameSettingsState} from '../shared/reducers/game-settings/game-settings.reducer';
import {Observable} from 'rxjs';
import {GameScoresState} from '../shared/reducers/game-scores/game-scores.reducer';
import {GameSettingsActions} from '../shared/actions/game-settings/game-settings.actions';
import {GameScoreActions} from '../shared/actions/game-score/game-score.actions';
import {AppState, gameScoreSelector, gameSettingsSelector} from '../shared/reducers/root';
import {Store} from '@ngrx/store';


@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.scss']
})
export class GameBoardComponent {
  storeParams$: Observable<GameSettingsState>;
  storePlayersParams$: Observable<GameScoresState>;
  state: GameSettingsState;
  scoresState: GameScoresState;

  constructor(private gameSettingActions: GameSettingsActions,
              private gameScoreActions: GameScoreActions,
              private store: Store<AppState>) {
    this.storeParams$ = store.select(gameSettingsSelector);
    this.storeParams$.subscribe((newState: GameSettingsState) => {
        this.state = newState;
    });
    this.storePlayersParams$ = store.select(gameScoreSelector);
    this.storePlayersParams$.subscribe((newState: GameScoresState) => {
        this.scoresState = newState;
    });
  }

  private updateState(newValue, action) {
    const state = action(newValue);
    this.store.dispatch(state);
  }
}
