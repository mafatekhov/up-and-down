import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';


import {
  MatButtonToggleModule,
  MatCheckboxModule,
  MatDialogModule,
  MatDividerModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSidenavModule,
  MatTableModule,
  MatTabsModule,
  MatChipsModule,
  MatIconModule,
  MatProgressBarModule,
  MatSliderModule,
  MatSortModule,
  MatSnackBarModule,
  MatSnackBar,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
  MAT_SNACK_BAR_DATA
} from '@angular/material';


@NgModule({
  imports: [
    MatButtonModule,
    MatMenuModule,
    MatInputModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatDividerModule,
    MatListModule,
    MatTabsModule,
    MatSelectModule,
    MatTableModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatChipsModule,
    MatIconModule,
    MatSliderModule,
    MatSortModule,
    MatSnackBarModule
  ],
  exports: [
    MatButtonModule,
    MatTooltipModule,
    MatMenuModule,
    MatInputModule,
    MatSidenavModule,
    MatCheckboxModule,
    MatDividerModule,
    MatListModule,
    MatTabsModule,
    MatSelectModule,
    MatTableModule,
    MatButtonToggleModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatChipsModule,
    MatIconModule,
    MatSliderModule,
    MatSortModule,
    MatSnackBarModule,
  ],
  providers: [
    MatSnackBar,
    {
      provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
      useValue: {
        duration: 2500
      }
    }
  ]
})
export class AppMaterialModule {
}
