import {PlayerScores} from '../models/player-score';


export function calculateNumberOfCardsAtHands(upTo, numberOfPlayers, currentRound) {
  return currentRound <= upTo ? currentRound :
    currentRound <= upTo + numberOfPlayers - 1 ? upTo :
      this.calculateNumberOfCardsAtHands(upTo, numberOfPlayers, currentRound - 1);
}

export function calculatePlayerScore(player: PlayerScores, bet, result) {
  const roundScore = bet === result ? 50 + result * 50 : result * 10;
  return player.getScore() + roundScore;
}

export function calculateNumberOfRounds(upTo, numberOfPlayers) {
  if (numberOfPlayers < 2) {
    return 0;
  }
  return (upTo - 1) * 2 + numberOfPlayers;
}
