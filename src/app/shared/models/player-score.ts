export class PlayerScores {
  private _player;
  private _score;

  constructor(player, score) {
    this._player = player;
    this._score = score;
  }

  getPlayer() {
    return this._player;
  }

  getScore() {
    return this._score;
  }

  setPlayer(val) {
    this._player = val;
  }

  setScore(val) {
    this._score = val;
  }
}
