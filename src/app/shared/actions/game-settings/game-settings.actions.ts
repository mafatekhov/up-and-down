import {Injectable} from '@angular/core';
import {UADAction} from '../up-and-down.action';

export const SET_NUMBER_OF_PLAYERS = 'SET_NUMBER_OF_PLAYERS';
export const SET_CARDS_UP_TO = 'SET_CARDS_UP_TO';
export const SET_NUMBER_OF_ROUNDS = 'SET_NUMBER_OF_ROUNDS';
export const SET_CURRENT_ROUND = 'SET_CURRENT_ROUND';


@Injectable()
export class GameSettingsActions {
  public setNumberOfPlayers(payload: number): UADAction {
    return {
      type: SET_NUMBER_OF_PLAYERS,
      payload
    };
  }
  public setCardsUpTo(payload: number): UADAction {
    return {
      type: SET_CARDS_UP_TO,
      payload
    };
  }
  public setNumberOfRounds(payload: number): UADAction {
    return {
      type: SET_NUMBER_OF_ROUNDS,
      payload
    };
  }
  public setCurrentRound(payload: number): UADAction {
    return {
      type: SET_CURRENT_ROUND,
      payload
    };
  }
}
