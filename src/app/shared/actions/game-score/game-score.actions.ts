import {Injectable} from '@angular/core';
import {PlayerScores} from '../../models/player-score';
import {UADAction} from '../up-and-down.action';

export const SET_PLAYERS_SCORES = 'SET_PLAYERS_SCORES';
export const ADD_PLAYER = 'ADD_PLAYER';
export const REMOVE_PLAYER = 'REMOVE_PLAYER';

@Injectable()
export class GameScoreActions {
  public setPlayers(payload: PlayerScores): UADAction {
    return {
      type: SET_PLAYERS_SCORES,
      payload
    };
  }

  public addPlayer(payload: PlayerScores): UADAction {
    return {
      type: ADD_PLAYER,
      payload
    };
  }

  public removePlayer(payload: PlayerScores): UADAction {
    return {
      type: REMOVE_PLAYER,
      payload
    };
  }
}
