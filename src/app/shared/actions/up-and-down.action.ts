import { Action } from '@ngrx/store';

export class UADAction implements Action {
  public type: string;
  public payload?: any;
}
