import * as R from 'ramda';
import {
  SET_CARDS_UP_TO, SET_CURRENT_ROUND, SET_NUMBER_OF_PLAYERS,
  SET_NUMBER_OF_ROUNDS
} from '../../actions/game-settings/game-settings.actions';
import {UADAction} from '../../actions/up-and-down.action';
import {calculateNumberOfRounds} from '../../services/game-scores.service';

export interface GameSettingsState {
  numberOfPlayers: number;
  cardsUpTo: number; // max cards to play in game
  numberOfRounds: number;
  currentRound: number;
}


export const initialState: GameSettingsState = {
  numberOfPlayers: 0,
  cardsUpTo: 1, // max cards to play in game
  numberOfRounds: 1,
  currentRound: 0,
};

export function GameSettingsReducer(state = initialState, action: UADAction): GameSettingsState {
  switch (action.type) {
    case SET_NUMBER_OF_PLAYERS: {
      const clonedObj = R.clone(state);
      clonedObj.numberOfPlayers = action.payload;
      clonedObj.numberOfRounds = calculateNumberOfRounds(clonedObj.cardsUpTo, action.payload);
      return clonedObj;
    }
    case SET_CARDS_UP_TO: {
      const clonedObj = R.clone(state);
      clonedObj.cardsUpTo = action.payload;
      clonedObj.numberOfRounds = calculateNumberOfRounds(action.payload, clonedObj.numberOfPlayers);
      return clonedObj;
    }
    case SET_CURRENT_ROUND: {
      const clonedObj = R.clone(state);
      clonedObj.currentRound = action.payload;
      return clonedObj;
    }
    default: {
      return state;
    }
  }
}

export const getGameSettingsState = (state: GameSettingsState) => state;
