import {GameScoresReducer, GameScoresState, getGameScoresState} from './game-scores/game-scores.reducer';
import {GameSettingsReducer, GameSettingsState, getGameSettingsState} from './game-settings/game-settings.reducer';
import {createSelector} from 'reselect';

export interface AppState {
  gameSettings: GameSettingsState;
  score: GameScoresState;
}

export const reducers = {
  gameSettings: GameSettingsReducer,
  score: GameScoresReducer
};

export const gameSettingsState = (state: AppState) => state.gameSettings;
export const gameSettingsSelector = createSelector(gameSettingsState, getGameSettingsState);

export const gameScoreState = (state: AppState) => state.score;
export const gameScoreSelector = createSelector(gameScoreState, getGameScoresState);


