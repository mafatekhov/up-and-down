import * as R from 'ramda';
import {PlayerScores} from '../../models/player-score';
import {Action} from '@ngrx/store';
import {ADD_PLAYER, REMOVE_PLAYER, SET_PLAYERS_SCORES} from '../../actions/game-score/game-score.actions';
import {UADAction} from '../../actions/up-and-down.action';


export interface GameScoresState {
  cardsAtHands: number;
  players: PlayerScores[];
}


export const initialState: GameScoresState = {
  cardsAtHands: 0,
  players: []
};


export function GameScoresReducer(state = initialState, action: UADAction): GameScoresState {
  switch (action.type) {
    case ADD_PLAYER: {
      const clonedObj = R.clone(state);
      clonedObj.players.push(action.payload);
      return clonedObj;
    }
    case REMOVE_PLAYER: {
      const clonedObj = R.clone(state);
      const pos = clonedObj.players.map((e) => e.getPlayer()).indexOf(action.payload.getPlayer());
      clonedObj.players.splice(pos, 1) ;
      return clonedObj;
    }
    default: {
      return state;
    }
  }
}

export const getGameScoresState = (state: GameScoresState) => state;
