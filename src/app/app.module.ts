import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {WelcomePageComponent} from './welcome-page/welcome-page.component';
import {AppMaterialModule} from './app-material.module';
import {NewGameComponent} from './new-game/new-game.component';
import {StoreModule} from '@ngrx/store';
import {
  reducers,
  AppState,
} from './shared/reducers/root';
import {GameSettingsActions} from './shared/actions/game-settings/game-settings.actions';
import {GameScoreActions} from './shared/actions/game-score/game-score.actions';
import {GameBoardComponent} from './game-board/game-board.component';


@NgModule({
  declarations: [
    AppComponent,
    NewGameComponent,
    GameBoardComponent,
    WelcomePageComponent
  ],
  imports: [
    AppRoutingModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers),
  ],
  providers: [
    GameSettingsActions,
    GameScoreActions,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
