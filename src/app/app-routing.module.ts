import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WelcomePageComponent} from './welcome-page/welcome-page.component';
import {NewGameComponent} from './new-game/new-game.component';
import {GameBoardComponent} from './game-board/game-board.component';

const routes: Routes = [
  {
    path: '',
    component: WelcomePageComponent,
    pathMatch: 'full'
  },
  {
    path: 'new-game',
    component: NewGameComponent,
    pathMatch: 'full'
  },
  {
    path: 'game-board',
    component: GameBoardComponent,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
